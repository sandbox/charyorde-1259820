var liveTweets = {};

// Last id fetched
liveTweets.last = null;

// Fetch new feeds and display them
liveTweets.update = function () {
	var query = "q=" + Drupal.settings.live_tweets.keywords || "Drupal";
	var ntweets = Drupal.settings.live_tweets.ntweets || 5;
	if (liveTweets.last) query += "&since_id=" + liveTweets.last;
	$.ajax({
		  url: "http://search.twitter.com/search.json",
		  cache: false,
		  dataType: "jsonp",
		  data: "rpp=" + ntweets + "&show_user=true&" + query,
		  success: function(json){
			if (!liveTweets.last) { 
				$('#block-live_tweets-0 .content').empty();
				$('#block-live_tweets-0').show();
			}
			var i;
			// Append tweets
			for (i = 0; i < json.results.length; i += 1) {
				if (liveTweets.last) {
					var tweets = $('#block-live_tweets-0 .content .tweet');
					if (tweets.length + 1 > ntweets) $(tweets.eq(tweets.length - 1)).remove();
					$('#block-live_tweets-0 .content').prepend(
					'<div class="tweet clearfix">' 
					+ '<div class="fusion-float-imagefield-left imagecache">' + liveTweets.parseAvatar(json.results[i].profile_image_url) + '</div>'
					+ '<div id="tweet-text">' + liveTweets.parse(json.results[i].text) + '</div>'
					+ '<div id="tweet-created">' + liveTweets.parseDate(json.results[i].created_at) + '</div>'
					+ '</div>');
				} else $('#block-live_tweets-0 .content').append('<div class="tweet">' + liveTweets.parseAvatar(json.results[i].profile_image_url) + liveTweets.parse(json.results[i].text) + '</div>');
			}
			// Store last tweet id
			if (json.results.length > 0) liveTweets.last = json.results[0].id;
		  }
		});
};

// Parse tweet feeds to create links
liveTweets.parse = function (text) {
		// Convert urls to links
		text = text.replace(/((http|https):\/\/[^ ]*)/g, '<a href="$1" target="_blank">$1</a> ');
		// @User links
		text = text.replace(/(^|\s)@(\w+)/g, '$1<a href="http://twitter.com/$2" target="_blank">@$2</a>');
		// #Search links
		text = text.replace(/(^|\s)#(\w+)/g, '$1<a href="http://search.twitter.com/search?q=%23$1" target="_blank">#$2</a>');
		return text
};

/**
 * Parse tweet feeds to display avatar
 * 
 */
liveTweets.parseAvatar = function(profile_image_url) {
	profile_image_url = '<img src="' + profile_image_url + '"/>';
	return profile_image_url;
}

/**
 * Converts to UTC date then prettyDate it.
 * 
 * Z = UTC time (To express the time of day in UTC)
 * T = local time (time of day in basic format)
 * 
 * @param holds Twitter date format of Sun, 24 Jul 2011 15:43:36 +0000
 */
liveTweets.parseDate = function(created_at) {
	//twitter_regex_date = /(\w+[,])\s(\d{2})\s(\w+)\s(\d{4})\s(\d{2}[:]\d{2}[:]\d{2})\s([+]\d{4})/;
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var pattern = /\s/;
	created_at = created_at.split(pattern);
	for (i = 0, len = created_at.length; i < len; i++){
		day_of_week = created_at[0];
		day = created_at[1];
		month_pos = created_at[2];
		month = 0 + months.indexOf(month_pos) + 1; // add 1 because array starts from zero
		year = created_at[3];
		time = created_at[4];
	}
	created_at = year+'-'+month+'-'+day+'T'+time+'Z'; //2011-07-24T19:18:357Z
	created_at = prettyDate(created_at);
	
	if(created_at != undefined)
	return created_at;
}

Drupal.behaviors.liveTweets = function(){ 
	if ($('#block-live_tweets-0').length) {
	liveTweets.update();
	setInterval( 'liveTweets.update()', Drupal.settings.live_tweets.refresh );
	}
//alert(liveTweets.parseDate("Sun, 24 Jul 2011 15:43:36 +0000"));
};


